# Kafka basics homework

To complete homework, I used the [Clickstream Data Analysis Pipeline Using ksqlDB tutorial](https://docs.confluent.io/platform/current/tutorials/examples/clickstream/docs/index.html?utm_source=github&utm_medium=demo&utm_campaign=ch.examples_type.community_content.clickstream). 
You can find all homework steps with screenshots below:

* Launched tutorial in docker

![launch](./images/launch.png)

* Simulating the stream of clicks and get values with ksql

![clickstream](./images/clickstream.png)

* View the three kafka-connect-datagen source connectors created with the ksqlDB CLI

![3gen](./images/3GEN.png)

* Verify the data

![flow1](./images/flow1.png)

![flow2](./images/flow2.png)

# Load the Clickstream Data in Grafana

* Dashboards with data

![graf1](./images/graf1.png)

![graf2](./images/graf2.png)

* Seven Elasticsearch sink connectors added (were created with the ksqlDB REST API).

![connect10](./images/connect10.png)

* The clickstream demo with simulating user sessions with a script. The script pauses the DATAGEN_CLICKSTREAM connector every 90 seconds for a 35 second period of inactivity. 

![graf3](./images/graf3.png)